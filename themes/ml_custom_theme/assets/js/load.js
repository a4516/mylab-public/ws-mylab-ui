var path = "/wp-content/themes/ee_custom_theme/assets/js/";


window.addEventListener("load", function () {
  jQuery
    .when(
      jQuery.getScript(path + "functions.js"),
      jQuery.getScript(path + "data-send.js"),
      jQuery.getScript(path + "modals.js"),

      jQuery.Deferred(function (deferred) {
        $(deferred.resolve);
      })
    )
    .done(function () {
    });
});

jQuery
  .when(
    jQuery.getScript(
      "https://cdn.jsdelivr.net/npm/getmdl-select@2.0.1/getmdl-select.min.js"
    ),
    jQuery.getScript(
      "https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.3.0/material.min.js"
    ),
    jQuery.getScript(
      "https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/js/iziModal.min.js"
    ),

    jQuery.Deferred(function (deferred) {
      $(deferred.resolve);
    })
  )
  .done(function () {
  });

