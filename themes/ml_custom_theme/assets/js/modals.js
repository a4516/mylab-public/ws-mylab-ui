var basePDFURL = "https://editorialepoca.mx/wp-content/themes/ee_custom_theme/assets/docs/catalogos/";

jQuery(document).on('click', '.apuntesEscolares', function (event) {
  event.preventDefault();
  jQuery('.apuntesEscolares').iziModal('open', this);
  jQuery(".c-pdf-modal-apuntesEscolares").iziModal({
    title: 'Catálogo - Apuntes Escolares',
    headerColor: '#a38f00',
    width: '90%',
    iframe: true,
    iframeURL: basePDFURL + 'CATALOGO_AE_2017_digital.pdf',
    iframeHeight: 800,
    zindex: 100000,

  });
});

jQuery(document).on('click', '.clasicosInfantiles', function (event) {
  event.preventDefault();
  jQuery('.clasicosInfantiles').iziModal('open', this);

  jQuery(".c-pdf-modal-clasicosInfantiles").iziModal({
    title: 'Catálogo - Clásicos Infantiles',
    headerColor: '#197301',
    width: '90%',
    iframe: true,
    iframeURL: basePDFURL + 'CATALOGO_CI_2017_digital.pdf',
    iframeHeight: 800,
    zindex: 100000,
  });
}); 

jQuery(document).on('click', '.divierteteAprende', function (event) {
  event.preventDefault();
  jQuery('.divierteteAprende').iziModal('open', this);

  jQuery(".c-pdf-modal-divierteteAprende").iziModal({
    title: 'Catálogo - Diviértete y aprende',
    headerColor: '#f14203',
    width: '90%',
    iframe: true,
    iframeURL: basePDFURL + 'CATALOGO_DYA_2017_digital.pdf',
    iframeHeight: 800,
    zindex: 100000,
  });
}); 


jQuery(document).on('click', '.editorialEpoca', function (event) {
  event.preventDefault();
  jQuery('.editorialEpoca').iziModal('open', this);

  jQuery(".c-pdf-modal-editorialEpoca").iziModal({
    title: 'Catálogo - Editorial Época',
    headerColor: '#437601',
    width: '90%',
    iframe: true,
    iframeURL: basePDFURL + 'CATALOGO_EE_2017_digital.pdf',
    iframeHeight: 800,
    zindex: 100000,
  });
}); 


jQuery(document).on('click', '.horus', function (event) {
  event.preventDefault();
  jQuery('.horus').iziModal('open', this);

  jQuery(".c-pdf-modal-horus").iziModal({
    title: 'Catálogo - Ediciones HORUS',
    headerColor: '#c902a4',
    width: '90%',
    iframe: true,
    iframeURL: basePDFURL + 'CATALOGO_H_2017_digital.pdf',
    iframeHeight: 800,
    zindex: 100000,
  });
}); 


jQuery(document).on('click', '.nuevoTalento', function (event) {
  event.preventDefault();
  jQuery('.nuevoTalento').iziModal('open', this);

  jQuery(".c-pdf-modal-nuevoTalento").iziModal({
    title: 'Catálogo - Nuevo Talento',
    headerColor: '#026bf6',
    width: '90%',
    iframe: true,
    iframeURL: basePDFURL + 'CATALOGO_NT_2017_digital.pdf',
    iframeHeight: 800,
    zindex: 100000,
  });
}); 


jQuery(document).on('click', '.rtm', function (event) {
  event.preventDefault();
  jQuery('.rmt').iziModal('open', this);

  jQuery(".c-pdf-modal-rtm").iziModal({
    title: 'Catálogo - Ediciones RTM',
    headerColor: '#da2502',
    width: '90%',
    iframe: true,
    iframeURL: basePDFURL + 'CATALOGO_RTM_2017_digital.pdf',
    iframeHeight: 800,
    zindex: 100000,
  });
}); 