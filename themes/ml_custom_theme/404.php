<?php /* Template Name: 404 */ ?>
<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!' );?>
<?php get_header(); ?>
<main class="main-content error-page">
    <p class="error-number">404</p>
    <p class="message">No pudimos encontrar esta página</p>
</main>
<?php get_footer(); ?>
