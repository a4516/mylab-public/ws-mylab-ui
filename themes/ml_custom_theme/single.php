<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!' );?>
<?php get_header(); ?>
<main class="main-content">
    <?php if ( have_posts() ) : ?>
    <?php
        while ( have_posts() ) : the_post(); ?>
        <div class="entry">
            <?php the_content(); ?>
        </div>
        <?php 
        endwhile; 
        else: ?>
            <p>No sentimos no hay información para este sitio</p>
        <?php endif; ?>
</main>
<?php get_footer(); ?>
